import Vue from 'vue';
import VueRouter from 'vue-router';
import DefaultLayout from '@/layouts/DefaultLayout';
import ContentLayout from '@/layouts/ContentLayout';
import AppHeader from '@/components/AppHeader';
import FormOne from '../components/forms/FormOne';
import FormSecond from '../components/forms/FormSecond';
import formsActions from '../data/formsActions';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: DefaultLayout,
    children: [
      {
        path: '',
        name: 'home',
        components: {
          header: AppHeader,
          default: ContentLayout,
        },
        props: {
          header: (route) => {
            const idx = formsActions.findIndex((f) => f.routerName === route.name);
            return {
              formAction: idx > -1 ? formsActions[idx].actionName : '',
            };
          },
        },
        children: [
          {
            path: 'form-1',
            name: 'formOne',
            components: {
              default: FormOne,
            },
          },
          {
            path: 'form-2',
            name: 'formSecond',
            components: {
              default: FormSecond,
            },
          },
        ],
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
