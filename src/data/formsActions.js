export default [
  {
    routerName: 'formOne',
    actionName: 'save-form-one',
  },
  {
    routerName: 'formSecond',
    actionName: 'save-form-second',
  },
];
